#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':

    volume, duration = 0, 0

    wip = list()
    for w in [32, 16, 8, 4]:
        wip += [Window('SpectralFullColumns', width=w)] * 4
    wii = list()
    for w in [8, 4]:
        # TODO how to define intensity window width
        wii += [Window('IntensityFullColumns', width=w)] * 4
    cr = CompressionRates(crSpectrum=1, crImage=1)

    for slid in ['2', '6', '4']:
        sl = Slit(slid)
        for wi in [wip, wii]:
            ex = Exposure(sl, wi, cr, 120 * u.s)
            ra = Raster(ex, 32, sl.width())
            st = Study(ra, 1)
            print(st)
            volume += st.dataVolume()
            duration += st.duration()
    # 3rd study for 4" slit
    ex = Exposure(sl, wip, cr, 120 * u.s)
    ra = Raster(ex, 32, sl.width())
    st = Study(ra, 1)
    print(st)
    volume += st.dataVolume()
    duration += st.duration()

    print('Data: {} ({}) in {} ({}): {}'.format(volume.to(u.MB), volume.to(
        u.MiB), duration, duration.to(u.hr), volume / duration))
