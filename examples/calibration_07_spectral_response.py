#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':

    volume, duration = 0, 0

    # windows including the 4 calibration lines – Mg IX, Ne VIII, C III, O VI), 3 on SW and 3 on LW
    wvl = [70.6, 77.0, 97.7, 103.2] * u.nm
    wi = [Window('SpectralFullColumns', width=32, wvl=w) for w in wvl]
    wi += [Window('SpectralFullColumns', width=32, detector=Detector('SW'))]
    wi += [Window('SpectralFullColumns', width=32, detector=Detector('LW'))]

    cr = CompressionRates(crSpectrum=1)
    dem = 'active_region'

    # independent parameters: slit ID and exposure time
    slids = ['2', '4', '6']
    exs = [40, 120, 360] * u.s

    for slid in slids:
        for ext in exs:
            sl = Slit(slid)
            ex = Exposure(sl, wi, cr, ext)
            ra = Raster(ex, 10, sl.width())
            st = Study(ra, 1)
            print(st)
            volume += st.dataVolume()
            duration += st.duration()
            if True:
                ax = plt.subplot(221)
                ex.plotSpectrum(0, ionList=['mg_9'],  demFile=dem, ax=ax)
                ax = plt.subplot(222)
                ex.plotSpectrum(1, ionList=['ne_8'], demFile=dem, ax=ax)
                ax = plt.subplot(223)
                ex.plotSpectrum(2, ionList=['c_3'], demFile=dem,
                                ax=ax)  # add2nd=True, if other ions
                ax = plt.subplot(224)
                ex.plotSpectrum(3, ionList=['o_6'], demFile=dem,
                                ax=ax)  # add2nd=True, if other ions
                fn = '/tmp/cal7_{}_{}.pdf'.format(slid, int(ex.exptime().value))
                print('Saving plot to', fn)
                plt.savefig(fn)
                plt.close()

    print('Data: {} ({}) in {} ({}): {}'.format(volume.to(u.MB), volume.to(
        u.MiB), duration, duration.to(u.hr), volume / duration))
