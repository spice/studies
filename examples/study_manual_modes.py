#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Studies from the user manual'''

from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *


class StudySpectralAtlas (Study):
    '''Spectral Atlas study (User Manual v8.0 p.45)'''

    def __init__(self):
        sl = Slit(4)
        wi = [Window('Full', detector=Detector('SW')), Window('Full', detector=Detector('LW'))]
        cr = CompressionRates(crSpectrum=20., crImage=10.)
        ex = Exposure(sl, wi, cr, 60 * u.s)
        ra = Raster(ex, 10, 4 * u.arcsec, dt=65 * u.s)
        Study.__init__(self, ra, 2)


class StudyComposition (Study):
    '''Composition Mapping study (User Manual v8.0 p.45)'''

    def __init__(self):
        sl = Slit(4)
        wi = [Window('Spectral')] * 2
        wi.extend([Window('Intensity')] * 13 * 2)  # intensity + background
        cr = CompressionRates(crSpectrum=20., crImage=10.)
        ex = Exposure(sl, wi, cr, 180 * u.s)
        ra = Raster(ex, 64, 4 * u.arcsec)
        Study.__init__(self, ra, 1)


class StudyDynamics (Study):
    '''Dynamics study (User Manual v8.0 p.45)'''

    def __init__(self):
        sl = Slit(2)
        wi = [Window('Spectral')] * 4
        wi.extend([Window('Intensity')] * 6 * 2)  # intensity + background
        cr = CompressionRates(crSpectrum=20., crImage=10.)
        ex = Exposure(sl, wi, cr, 5 * u.s)
        ra = Raster(ex, 128, 2 * u.arcsec, dt=5.3 * u.s)
        Study.__init__(self, ra, 10)


if __name__ == '__main__':
    studies = [StudySpectralAtlas(), StudyComposition(), StudyDynamics()]

    for st in studies:
        print(st.__class__.__name__)
        print(st)
        print('Data: {} in {}: {}\n'.format(st.dataVolume(), st.duration(), st.meanDataRate()))
