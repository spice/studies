#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# For co-registration observations during RSCWs.
# Discussed at RSWG on 2019-07-09 (SOWG14)

if __name__ == '__main__':
    sl = Slit(2)
    wi = [Window('Spectral', width=32, detector=Detector('SW'), slit=sl)] * 3
    wi += [Window('Spectral', width=32, detector=Detector('LW'), slit=sl)] * 3
    wi += [Window('SpectralDumbbells', width=32, detector=Detector('LW'), slit=sl)]
    cr = CompressionRates(crSpectrum=1)
    ex = Exposure(sl, wi, cr, 10 * u.s)
    ra = Raster(ex, 256, 2 * u.arcsec)
    st = Study(ra, 4)
    print(st)
    print('Data: {:.6}'.format(st.dataVolume().to(u.Mb)))
