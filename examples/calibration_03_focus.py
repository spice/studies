#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':

    volume, duration = 0, 0
    for s in ['2', '30']:
        sl = Slit(s)
        wi = [Window('SpectralFullColumns', width=32, detector=Detector('SW'), slit=sl)] * 3
        wi += [Window('SpectralFullColumns', width=32, detector=Detector('LW'), slit=sl)] * 3
        cr = CompressionRates(crSpectrum=1)
        if s == '2':
            exptime = 120 * u.s
        else:
            exptime = 40 * u.s
        ex = Exposure(sl, wi, cr, exptime)
        ra = Raster(ex, 2, 0)
        st = Study(ra, 13)

        print(st)
        volume += st.dataVolume()
        duration += st.duration()

    print('Data: {} ({}) in {} ({}): {}'.format(volume.to(u.MB), volume.to(
        u.MiB), duration, duration.to(u.hr), volume / duration))
