#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':

    volume, duration = 0, 0
    for s in ['4', '6']:
        sl = Slit(s)
        wi = [Window('Full', detector=Detector('SW')),
              Window('Full', detector=Detector('LW'))]
        cr = CompressionRates(crImage=1)
        if s == '2' or s == '4':
            exptime = 120 * u.s
        else:
            exptime = 60 * u.s
        ex = Exposure(sl, wi, cr, exptime)
        ra = Raster(ex, 1, 0)
        st = Study(ra, 1)

        print(st)
        volume += st.dataVolume()
        duration += st.duration()

    print('Data: {} ({}) in {} ({}): {}'.format(volume.to(u.MB), volume.to(
        u.MiB), duration, duration.to(u.hr), volume / duration))
