#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

if __name__ == '__main__':
    sl = Slit(4)
    wi = [
        Window('Spectral', wvl=77.04*u.nm),  # Ne viii
        Window('Spectral', wvl=103.2*u.nm),  # O vi
        Window('Spectral', wvl=103.7*u.nm),  # O vi
        Window('Spectral', wvl=104.2*u.nm),  # Si xii 2nd order
    ]
    wi.extend([Window('Spectral')] * 2)
    wi.extend([Window('Intensity')] * 4)
    cr = CompressionRates()
    ex = Exposure(sl, wi, cr, 4 * u.s)
    ra = Raster(ex, 100, sl.width())
    st = Study(ra, 5)

    print(st)
    print('Data: {} in {}: {}'.format(st.dataVolume(), st.duration(), st.meanDataRate()))
    dem = 'active_region'
    ex.plotSpectrum(0, ionList=['ne_8'],  demFile=dem, add2nd=True)
    ex.plotSpectrum(1, ionList=['o_6'],   demFile=dem, add2nd=True)
    ex.plotSpectrum(2, ionList=['o_6'],   demFile=dem, add2nd=True)
    ex.plotSpectrum(3, ionList=['si_12'], demFile=dem, add2nd=True)  # not in SUMER range
