#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':

    # 4 calibration lines – Mg IX, Ne VIII, C III, O VI), 3 on SW and 3 on LW
    wvl = [70.6, 77.0, 97.7, 103.2] * u.nm
    wi = [Window('Spectral', width=32, wvl=w) for w in wvl]
    wi += [Window('Spectral', width=32, detector=Detector('SW'))]
    wi += [Window('Spectral', width=32, detector=Detector('LW'))]

    # parameters of the 6 studies
    slids = ['6', '2', '2', '2', '2', '6']
    crs = [1, 1, 4, 10, 20, 20]

    rasteps = [{'2': 96, '6': 32}, {'2': 192, '6': 64}]
    radxs = {'2': 2 * u.arcsec, '6': 6 * u.arcsec}

    for ras in rasteps:
        volume, duration = 0, 0
        for i in range(len(slids)):
            sl = Slit(slids[i])
            cr = CompressionRates(crSpectrum=crs[i])
            ex = Exposure(sl, wi, cr, 60 * u.s)
            ra = Raster(ex, ras[slids[i]], radxs[slids[i]])
            st = Study(ra, 1)
            print(st)
            volume += st.dataVolume()
            duration += st.duration()

        print('Data: {} in {}: {}'.format(volume.to(u.MB), duration, volume / duration))
    print('\n')
