#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':

    volume, duration = 0, 0

    # first 4 studies, and 5th study
    slids = ['2', '4', '6', '30', '2']
    crs = [10, 10, 10, 1, 10]
    exs = [120, 120, 60, 40, 5] * u.s
    rasteps = [128, 64, 160, 32, 128]
    radxs = [2, 4, 6, 30, 2] * u.arcsec
    for i in range(len(slids)):
        sl = Slit(slids[i])
        wi = [Window('SpectralFullColumns', width=32, detector=Detector('SW'), slit=sl)] * 3
        wi += [Window('SpectralFullColumns', width=32, detector=Detector('LW'), slit=sl)] * 3
        cr = CompressionRates(crSpectrum=crs[i])
        ex = Exposure(sl, wi, cr, exs[i])
        ra = Raster(ex, rasteps[i], radxs[i])
        st = Study(ra, 1)
        print(st)
        volume += st.dataVolume()
        duration += st.duration()

    print('Data: {} ({}) in {} ({}): {}'.format(volume.to(u.MB), volume.to(
        u.MiB), duration, duration.to(u.hr), volume / duration))

# TODO much smaller than in Alessandra's document
# TODO question: full detector columns, or only slit length? Dumbells?
