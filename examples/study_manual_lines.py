#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

import astropy.units as u
import numpy as np

import SPICEstudies

if __name__ == '__main__':
    wl = np.array([49.9 * 2, 52.1 * 2, 70.602, 72.123, 76.043, 77.042, 77.231,
                   78.03, 78.647, 78.772, 97.335, 97.5, 97.703, 99.7, 100.579, 102.572,
                   102.8, 103.193, 103.634, 103.764, 104.925]) * u.nm
    els = ['h', 'he', 'li', 'be',  'b',  'c',  'n',  'o',  'f', 'ne', 'na', 'mg',
           'al', 'si',  'p',  's', 'cl', 'ar',  'k', 'ca', 'sc', 'ti',  'v', 'cr',
           'mn', 'fe', 'co', 'ni', 'cu', 'zn']
    for w in wl:
        wi = SPICEstudies.Window('Spectral', wvl=w)
        print(wi)
        wi.plotSpectrum(slit=SPICEstudies.Slit(2), demFile='active_region',
                        elementList=els, add2nd=(w.to(u.nm).value > 79.5))
