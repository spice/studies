#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':
    wi = [Window('Full', detector=Detector('SW')),
          Window('Full', detector=Detector('LW'))]
    sl = Slit('30')
    cr = CompressionRates(crImage=10)
    ex = Exposure(sl, wi, cr, 60 * u.s)
    ra = Raster(ex, 32, sl.width())
    st = Study(ra, 1)
    print(st)
