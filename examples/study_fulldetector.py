#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

import astropy.units as u
import matplotlib.pyplot as plt

import SPICEstudies

if __name__ == '__main__':
    sl = SPICEstudies.Slit(2)
    wi = [
        SPICEstudies.Window('Full', detector=SPICEstudies.Detector('SW')),
        SPICEstudies.Window('Full', detector=SPICEstudies.Detector('LW')),
    ]
    cr = SPICEstudies.CompressionRates()
    ex = SPICEstudies.Exposure(sl, wi, cr, 4 * u.s)
    ra = SPICEstudies.Raster(ex, 1, 0)
    st = SPICEstudies.Study(ra, 1)

    print(st)
    print('Data: {} in {}: {}'.format(st.dataVolume(), st.duration(), st.meanDataRate()))

    dems = ['coronal_hole', 'quiet_sun', 'active_region']  # , 'flare']

    els = ['h', 'he', 'li', 'be',  'b',  'c',  'n',  'o',  'f', 'ne', 'na', 'mg',
           'al', 'si',  'p',  's', 'cl', 'ar',  'k', 'ca', 'sc', 'ti',  'v', 'cr',
           'mn', 'fe', 'co', 'ni', 'cu', 'zn']

    # pre-compute, without plotting
    # for dem in dems:
    #wi[0].spectrum (slit=sl, demFile=dem, elementList=els)
    #wi[1].spectrum (slit=sl, demFile=dem, elementList=els, add2nd=True)

    # spectrum on both detector arrays for different DEMs
    if True:
        ax = plt.subplot(211)
        for dem in dems:
            #wi[0].plotSpectrum (slit=SPICEstudies.Slit(2), demFile=dem, elementList=els, ax=ax)
            wi[0].plotSpectrum(slit=SPICEstudies.Slit(4), demFile=dem, elementList=els, ax=ax)
        ax = plt.subplot(212)
        for dem in dems:
            #wi[1].plotSpectrum (slit=SPICEstudies.Slit(2), demFile=dem, elementList=els, add2nd=True, ax=ax)
            wi[1].plotSpectrum(slit=SPICEstudies.Slit(4), demFile=dem,
                               elementList=els, add2nd=True, ax=ax)
        plt.show()

    # compare 4" slit spectrum and (double of) 2" slit spectrum
    if False:
        ax = plt.subplot()
        wi[0].plotSpectrum(slit=SPICEstudies.Slit(2), demFile='active_region',
                           elementList=els, ax=ax, factor=2, color='k', linestyle='-', marker=None)
        wi[0].plotSpectrum(slit=SPICEstudies.Slit(4), demFile='active_region',
                           elementList=els, ax=ax, color='k', linestyle='--', marker=None)
        plt.show()
