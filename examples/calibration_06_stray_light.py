#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import

from SPICEstudies import *

# Calibration study following
# SPICE in-flight calibration activities - summary
# Alessandra Giunta, Tim Grundy
# 03/07/2017


if __name__ == '__main__':
    wi = [Window('SpectralFullColumns', width=32, detector=Detector('SW'))] * 3
    wi += [Window('SpectralFullColumns', width=32, detector=Detector('LW'))] * 3
    sl = Slit('2')
    cr = CompressionRates(crSpectrum=1)
    ex = Exposure(sl, wi, cr, 360 * u.s)
    ra = Raster(ex, 5, 96 * u.arcsec)
    st = Study(ra, 2)
    print(st)
