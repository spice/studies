# SPICEstudies

## Description

SPICEstudies is a Python package to build SO SPICE studies, and compute their data rates and count rates, based on the SPICE User Manual and the CHIANTI atomic physics data base.


## Dependencies

This tool relies on the ChiantiPy CHIANTI interface (tested with ChiantiPy 0.6.1 and CHIANTI 8.0.1).
It also uses numpy, astropy.units, matplotlib (mandatory), and astroquery.vizier (optional).

It should run with Python 2 as well as with 3.

## How to use it

* Clone the git repository to some directory and rename it:

        git clone https://git.ias.u-psud.fr/spice/studies.git
        mv studies SPICEstudies

* Install the package:

        pip install SPICEstudies

* To use the module from (i)python:

        import SPICEstudies

* To run an example file in ipython:

        %run SPICEstudies/study_example

* To define the studies from the User Manual:

        %run SPICEstudies/study_user_manual


## TODO

Functionalities:

* Check accuracy of counts
* Instantiates the studies for the different modes: Full Spectrum, Spatial Scan... (see user manual p. 25+)
* Better estimates of durations and data volumes (overheads, bits per pixel); take into account orbit, slit, calibration... for count rates.
* Identify lines on spectra, and/or line identifications (SUMER spectral atlas...)

Other developments:

* Documentation. Describe dependencies (CHIANTI)
* Make work on IAS servers
* GUI
