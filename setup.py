from setuptools import setup

setup(
    name = "SPICEstudies",
    version = "0.1",
    author = "Eric Buchlin",
    author_email = "eric.buchlin@universite-paris-saclay.fr",
    description = "SPICE studies modelling",
    license = "GPLv3",
    keywords = "Solar_Orbiter SPICE Sun spectroscopy",
    url = "https://git.ias.u-psud.fr/spice/studies",
    packages = ["SPICEstudies"],
    long_description = "See README.md",
    classifiers = [
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public", "License v3 (GPLv3)",
        "Programming Language :: Python :: 3"
        "Topic :: Scientific/Engineering :: Astronomy",
        ]
)
