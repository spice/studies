#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .exposure import Exposure
from .aux import indent

import re


class Raster:
    '''A SPICE raster'''

    def __init__(self, exposure, nsteps, dx, dt=None):
        assert (isinstance(exposure, Exposure))
        assert (nsteps >= 0)
        if dt is None:
            dt = exposure.duration()  # TODO + slit motion duration
        assert (dt >= exposure.duration())

        self.exposure = exposure
        self.nsteps = nsteps
        self.dx = dx
        self.dt = dt

    def duration(self):
        '''Study duration'''
        return self.nsteps * self.dt

    def dataVolume(self):
        '''Raster data volume'''
        return self.nsteps * self.exposure.dataVolume()

    def meanDataRate(self):
        '''Study mean data rate'''
        return self.dataVolume() / self.duration()

    def __str__(self):
        '''String for printing raster information'''
        s = 'Raster of {} exposures, step size {}, of:\n'.format(self.nsteps, self.dx)
        s += indent(self.exposure.__str__())
        return s
