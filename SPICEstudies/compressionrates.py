#!/usr/bin/env python
# -*- coding: utf-8 -*-

import warnings


class CompressionRates:
    '''Compression rates'''

    def __init__(self, crSpectrum=20., crImage=10.):
        assert (crSpectrum > 0. and crImage > 0.)
        if crSpectrum < 1. or crSpectrum > 30.:
            warnings.warn('Suspicious spectrum compression rate (not in [1, 30])')
        if crImage < 1. or crImage > 20.:
            warnings.warn('Suspicious image compression rate (not in [1, 20])')
        self.spectrum = crSpectrum
        self.image = crImage

    def __str__(self):
        '''String for printing compression rates information'''
        return 'Compression rates: {} for spectra, {} for images'.format(
            self.spectrum, self.image
        )
