#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
from scipy.io.idl import readsav
import astropy.units as u
import astropy.constants as const


class Detector:
    '''Detector/instrument characteristics'''
    # for each detector: magnification factors, dispersion, total and used pixels,
    # first used pixel coordinates, wavelength of first used pixel,
    # pixel physical size, spectral resolving power, bolometric effective area
    detectors = {'SW': {'mag': (18.39, 16.34) * u.um / u.arcsec,
                        'disp': 0.0095 * u.nm / u.pix,
                        'pixels': (1024, 1024),
                        'usedpix': (968, 800),
                        'startpix': (28, 112),
                        'wvl0': 70.104 * u.nm,
                        'pixsize': 18 * u.um / u.pix,
                        'spectres': 2170.,   # for 2.5arcsec slit, SPICE-RP-0002 8.3.1.4
                        },
                 'LW': {'mag': (20.83, 17.00) * u.um / u.arcsec,
                        'disp': 0.0083 * u.nm / u.pix,
                        'pixels': (1024, 1024),
                        'usedpix': (968, 800),
                        'startpix': (28, 112),
                        'wvl0': 97.076 * u.nm,
                        'pixsize': 18 * u.um / u.pix,
                        'spectres': 3230.,   # for 2.5arcsec slit, SPICE-RP-0002 8.3.1.4
                        }
                 }
    # Is startpix a characteristic of the detector or of the exposure?
    # Is usepix useful?

    @staticmethod
    def wvl2detector(wvl):
        '''Find which detector (if any) includes given wavelength

        Keyword arguments:
        wvl -- Wavelength (in any length unit)
        '''
        for id in Detector.detectors.keys():
            d = Detector.detectors[id]
            l0 = d['wvl0']
            l1 = l0 + d['usedpix'][0] * u.pix * d['disp']
            if wvl >= l0 and wvl <= l1:
                return id
        raise RuntimeError('Wavelength not in any detector\'s range')

    def __init__(self, id):
        '''Define detector

        Keyword arguments:
        id -- detector identifier (string)
        '''
        self.set_id(id)

    def set_id(self, id):
        if id not in Detector.detectors.keys():
            raise ValueError('Detector should be one of ' +
                             ' '.join([str(s) for s in Detector.detectors.keys()]))
        self._id = id

    def mag(self, pix=False):
        '''Return instrument magnification: image size on detector per unit
        observing angle, in both detector dimensions

        Keyword arguments:
        pix -- if True, result is in pixels (e.g. pix/arcsec) instead of
          length units (e.g. um/arcsec)
        '''
        m = u.Quantity(Detector.detectors[self._id]['mag'])  # copy value, not reference
        if pix:
            m /= Detector.detectors[self._id]['pixsize']
        return m

    def magx_px(self): return self.mag(pix=True)[0]

    def magy_px(self): return self.mag(pix=True)[1]

    def disp(self):
        '''Return instrument dispersion (wavelength per pixel)'''
        return Detector.detectors[self._id]['disp']

    def pixels(self):
        '''Return detector size in pixels'''
        return Detector.detectors[self._id]['pixels']

    def usedPix(self):
        '''Return used pixels on detector'''
        return Detector.detectors[self._id]['usedpix']

    def startPix(self):
        '''Return first used pixel coordinates'''
        return Detector.detectors[self._id]['startpix']

    def wvl0(self):
        '''Return wavelength of first used pixel'''
        return Detector.detectors[self._id]['wvl0']

    @staticmethod
    @u.quantity_input
    def effectiveArea(wvl: u.nm):
        '''
        Get the SPICE effective area for some wavelength
        
        Parameters
        ----------
        wvl: Quantity
            Wavelength (or array of wavelengths)

        Return
        ------
        Quantity:
            Effective area(s)
        '''
        wvl = wvl.to(u.nm).value
        f = os.path.join(os.path.dirname(__file__),
                            'calibration-data', 'effective_area.sav')
        a = readsav(f)  # TODO: should not be read at each call
        # try interpolating for both second (1) and first (2) order
        aeff1 = np.interp(wvl, a['lam_1'], a['net_resp_1'], left=np.nan, right=np.nan)
        aeff2 = np.interp(wvl, a['lam_2'], a['net_resp_2'], left=np.nan, right=np.nan)
        # choose where interpolation was done for the correct order
        return np.where(np.isfinite(aeff2), aeff2, aeff1) * u.mm**2
    
    @staticmethod
    @u.quantity_input
    def quantumEfficiency(wvl: u.Angstrom):
        '''
        Get the SPICE detector quantum efficiency for some wavelength
        
        Parameters
        ----------
        wvl: Quantity
            Wavelength (or array of wavelengths)

        Return
        ------
        Quantity:
            Quantum efficiency(ies): number of detected photons / number of incident photons
        '''
        wvl = wvl.to(u.Angstrom).value
        # Source: Table 8.8 of SPICE-RAL-RP-0002 v10.0
        qe_sw = np.interp(wvl,
                        [703, 706, 770, 790],   # angstrom
                        [0.12, 0.12, 0.1, 0.1],  # electron/photon
                        left=np.nan, right=np.nan)
        qe_lw = 0.25 # electron/photon
        return np.where((wvl > 703) & (wvl < 791), qe_sw, qe_lw)

    def spectres(self):
        '''Return instrument spectral resolution (resolving power)'''
        return Detector.detectors[self._id]['spectres']

    # TODO other accessors...?

    def wvlRange(self, fullArray=False, order=1):
        '''Return wavelength range on detector array (only on used part, if fullArray=False).
        This corresponds to central wavelengths of all pixels (first and last inclusive).
        order: 1 for first order, 2 for 2nd-order'''
        d = Detector.detectors[self._id]
        if fullArray:
            l0 = self.pix2wvl(0)
            l1 = self.pix2wvl(d['pixels'][0] - 1)
        else:
            l0 = self.pix2wvl(d['startpix'][0])
            l1 = self.pix2wvl(d['startpix'][0] + d['usedpix'][0] - 1)
        return l0 / order, l1 / order

    def pix2wvl(self, p, allowOutArray=False):
        '''Return wavelength corresponding to a pixel index on detector

        Assumes a linear dispersion as given by the user manual.

        Keyword arguments:
        p -- pixel index in x/λ direction (value, or quantity in pixels)
        allowOutArray -- If True, allow that pixel is not on array
        '''
        if not allowOutArray:
            assert (p >= 0 and p < self.pixels()[0])
        return self.wvl0() + (p - self.startPix()[0]) * u.pix * self.disp()

    def wvl2pix(self, wvl):
        '''Return pixel index corresponding to some wavelength

        Assumes a linear dispersion as given by the user manual

        Keyword arguments:
        l -- wavelength (in any length unit)
        '''
        r = self.wvlRange()
        if wvl < r[0] or wvl > r[1]:
            raise ValueError('Wavelength not in detector wavelength range')
        return self.startPix()[0] + ((wvl - self.wvl0()) / self.disp()).to(u.pix).value

    def __str__(self):
        '''String for printing detector information'''
        return 'Detector: {}'.format(self._id)
