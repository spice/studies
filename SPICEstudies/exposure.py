#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np

from .slit import Slit
from .compressionrates import CompressionRates
from .window import *


class Exposure:
    '''A SPICE exposure (or raster step)'''

    def __init__(self, slit, windows, compressionRates, exptime):
        self.set_slit(slit)
        self.set_compressionRates(compressionRates)
        self.set_exptime(exptime)
        self._set_windows(windows)

    # maximum number of windows and spectral pixels

    @classmethod
    def maxWindows(self): return 32

    @classmethod
    def maxSpecPix(self): return 256

    # setters

    def set_slit(self, slit):
        assert (isinstance(slit, Slit))
        self._slit = slit

    def set_compressionRates(self, compressionRates):
        assert (isinstance(compressionRates, CompressionRates))
        self._compressionRates = compressionRates

    def set_exptime(self, exptime):
        assert (isinstance(exptime, u.Quantity))
        assert (exptime >= 0)
        self._exptime = exptime

    def _set_windows(self, windows):
        '''Set windows and check constraints on list of windows for SPICE exposure (User Manual 2.2.2.2)'''
        self._windows = windows
        assert (type(self._windows) == list)
        # for w in self._windows:
        # assert (isinstance (w, Window))  # TODO Window not known here??

        # 1. check that windows do not overlap on each detector
        for d in Detector.detectors.keys():
            ranges = [w.pixRange for w in self._windows if hasattr(
                w, 'pixRange') and w.detector == d]
            ranges.sort()  # sort by first element of each pair
            for i in range(len(ranges) - 1):
                if ranges[i][1] > ranges[i+1][0]:
                    raise RuntimeError('Windows should not overlap')  # TODO be more informative

        # 2. check there are pairs of regions (2 windows) for each intensity
        # This is actually not an instrumental constraint

        # 3. maximum number of windows
        if len(self._windows) > self.maxWindows():
            raise ValueError(
                'Number of windows should be no more than {}'.format(self.maxWindows()))

        # 4, 8, 10: checked at initialization of Window
        # 9.: ystart is now in Detector, but it should be moved in Window; then
        #  check that all windows have the same ystart

        # 5. total output width <= 256 (or 128 if double exposure) (if not Full spectrum)
        if any([w.type != 'Full' for w in self._windows]):
            specPix = 0
            for w in self._windows:
                if w.type == 'Intensity':
                    specPix += 1
                else:
                    specPix += w.width
            if specPix > self.maxSpecPix():
                raise ValueError(
                    'Total number of lambda pixels should be no more than {}'.format(self.maxSpecPix()))
        elif len(self._windows) > 2:
            raise ValueError('Number of Full spectrum windows should be no more than 2')

        # 6. is not a requirement
        # 7. only one window with dumbbells
        if np.sum([(w.dbn > 0) for w in self._windows]) > 1:
            raise (ValueError('Number of windows with dumbells should be no more than 1'))

    def set_windows(self, windows):
        '''Set windows and check constraints on list of windows for SPICE exposure (User Manual 2.2.2.2)'''
        self._set_windows(windows)

    # getters

    def slit(self): return self._slit

    def compressionRates(self): return self._compressionRates

    def exptime(self): return self._exptime

    def exptimes(self): return [self._exptime]

    def windows(self): return self._windows

    def duration(self):
        '''Exposure duration'''
        return self._exptime

    def dataVolume(self):
        '''Exposure data volume'''
        dv = 0
        for w in self._windows:
            dv += w.dataVolume(self._compressionRates)
        return dv

    def plotSpectrum(self, iwindow, elementList=None, ionList=None, demFile=False, add2nd=False, logy=False, ax=None):
        assert (iwindow >= 0 and iwindow < len(self._windows))
        self._windows[iwindow].plotSpectrum(slit=self._slit, exptime=self._exptime,
                                            elementList=elementList, ionList=ionList, demFile=demFile,
                                            add2nd=add2nd, logy=logy, ax=ax)

    def plotSpectra(self, elementList=None, ionList=None, demFile=None, add2nd=False, ncols=2, logy=False):
        iwindows = [i for i in range(len(self._windows)) if hasattr(self._windows[i], 'wvlCentre')]
        if len(iwindows) == 0:
            warnings.warn('No window with position information has been found')
            return
        nrows = (len(iwindows) - 1) // ncols + 1
        fig, ax = plt.subplots(nrows=nrows, ncols=ncols)
        for i, a in enumerate(ax.flat):
            self._windows[i].plotSpectrum(slit=self._slit, exptime=self._exptime,
                                          elementList=elementList, ionList=ionList, demFile=demFile,
                                          add2nd=add2nd, ax=a, logy=logy)
        plt.show()

    def __str__(self):
        '''String for printing exposure information'''
        s = 'Exposure time {}\n'.format(self._exptime)
        s += '  ' + self._slit.__str__() + '\n'
        s += '  ' + self._compressionRates.__str__() + '\n'
        for w in self._windows:
            s += '  ' + w.__str__() + '\n'
        return s  # [:-1]  # remove last \n


class DoubleExposure (Exposure):

    def __init__(self, slit, windows, compressionRates, exptimes):
        Exposure.__init__(self, slit, windows, compressionRates, exptimes[0])
        self.set_exptime2(exptimes[1])

    # maximum number of windows and spectral pixels
    @classmethod
    def maxWindows(self): return 16

    @classmethod
    def maxSpecPix(self): return 128

    # setters (see also parent class)
    def set_exptime2(self, exptime2):
        assert (isinstance(exptime2, u.Quantity))
        assert (exptime2 >= 0)
        self._exptime2 = exptime2

    def set_windows(self, windows):
        '''Set windows and check constraints on list of windows for SPICE exposure (User Manual 2.2.2.2)'''
        self._set_windows(windows)

    # getters (see also parent class)
    def exptime2(self): return self._exptime2

    def exptimes(self): return [self._exptime, self._exptime2]

    def duration(self): return self._exptime + self._exptime2

    def dataVolume(self): return 2 * Exposure.dataVolume(self)

    def __str__(self):
        '''String for printing exposure information'''
        s = 'Exposure time {} and {}\n'.format(self._exptime, self._exptime2)
        s += '  ' + self._slit.__str__() + '\n'
        s += '  ' + self._compressionRates.__str__() + '\n'
        for w in self._windows:
            s += '  ' + w.__str__() + '\n'
        return s  # [:-1]  # remove last \n
