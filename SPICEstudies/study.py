#!/usr/bin/env python
# -*- coding: utf-8 -*-
import astropy.units as u

from .raster import Raster
from .aux import indent


class Study:
    '''A SPICE study'''

    def __init__(self, raster, repeats):
        #assert (isinstance (raster, Raster))
        self.raster = raster
        self.repeats = repeats

    def duration(self):
        '''Study duration'''
        # TODO: add overheads in the cases of different studies
        return self.repeats * self.raster.duration()

    def dataVolume(self):
        '''Study data volume'''
        return self.repeats * self.raster.dataVolume()

    def meanDataRate(self):
        '''Study mean data rate'''
        return self.raster.meanDataRate()

    def __str__(self):
        '''String for printing study information'''
        s = 'Study of {} repeat{} of:\n'.format(self.repeats, 's' if self.repeats > 1 else '')
        s += indent(self.raster.__str__())
        s += 'Data: {:.6} ({:.6}) in {:.6} ({:.6}): {:.8}\n'.format(self.dataVolume().to(u.MB), self.dataVolume(
        ).to(u.MiB), self.duration(), self.duration().to(u.hr), self.meanDataRate().to(u.b/u.s))
        return s


# TODO: subclasses for studies in different operations modes

# TODO: build a study from parameters (without using slit object, etc.)
