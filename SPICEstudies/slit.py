#!/usr/bin/env python
# -*- coding: utf-8 -*-

import astropy.units as u


class Slit:
    '''A SPICE slit'''
    slits = {'2': {'width': 2.5 * u.arcsec, 'length': 11.04 * u.arcmin, 'dumbbell': True},
             '4': {'width': 4.0 * u.arcsec, 'length': 11.04 * u.arcmin, 'dumbbell': True},
             '6': {'width': 6.0 * u.arcsec, 'length': 11.04 * u.arcmin, 'dumbbell': True},
             '30': {'width': 30.0 * u.arcsec, 'length': 14.0 * u.arcmin, 'dumbbell': False}
             }
    dbsize = (30, 30) * u.arcsec

    def __init__(self, id):
        '''
        Define slit

        Parameters
        ----------
        id: string
            Slit identifier, corresponding to the approximate slit width in arcsec
        '''
        self.set_id(id)

    def set_id(self, id):
        '''
        Set slit id
        '''

        id = str(id)   # make sure that we have a string identifier (then argument can also be integer)
        if id not in Slit.slits.keys():
            raise ValueError('Slit should be one of ' +
                             ' '.join([str(s) for s in Slit.slits.keys()]))
        self._id = id

    def parameters(self):
        '''
        Return all slit parameters

        Return
        -------
        dict:
            Slit parameters
        '''
        return Slit.slits[self._id]

    def width(self):
        '''
        Return slit width

        Return
        ------
        Quantity (angle):
            Slit width
        '''
        return Slit.slits[self._id]['width']

    def length(self):
        '''
        Return slit length

        Return
        ------
        Quantity (angle):
            Slit length
        '''
        return Slit.slits[self._id]['length']

    def dumbbell(self):
        '''
        Return dumbbell width and height if slit has dumbbells, or None

        Return
        ------
        Quantity (angle) or NoneType:
            Dumbbell width and height
        '''
        if Slit.slits[self._id]['dumbbell']:
            return Slit.dbsize
        else:
            return None

    def __str__(self):
        '''
        String for printing slit information

        Return
        ------
        string:
            Slit information
        '''
        return 'Slit {} ({} x {})'.format(
            self._id,
            self.width().to(u.arcsec),
            self.length(). to(u.arcmin)
        )
