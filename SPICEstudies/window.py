#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import warnings
import matplotlib.pyplot as plt
import scipy.integrate
import os
import astropy.units as u
import astropy.constants as const

from .detector import Detector
from .slit import Slit
from .__init__ import cachePath
from .aux import getCurdt01Atlas


def symmetricOnes(width):
    '''Build a symmetric kernel of ones (non-normalized) of some width, with an
    odd number of ones, and the remainder split between both ends'''
    widthOdd = int(np.floor((width - 1.) / 2.)) * 2 + 1  # previous odd integer
    k = [(width - widthOdd) / 2.]
    k.extend([1.] * widthOdd)
    k.append(k[0])
    return k


class Window:
    '''A data window on a SPICE detector'''
    # static members
    # Allowed window types and shapes
    allowedTypes = ['Full', 'Spectral', 'SpectralFullColumns',
                    'SpectralDumbbells', 'Dumbbells', 'Intensity', 'IntensityFullColumns']
    allowedWidths = [4, 8, 16, 32]  # except for "Full"

    def __init__(self, type='Spectral', wvl=None, width=None, height=None,
                 detector=Detector('SW'), slit=Slit(4), xbin=1):
        '''Define detector window

        Keyword arguments:
        type -- Window type
        wvl -- Central wavelength (ignored for 'Full')
        width -- Window width (ignored for type='Full')
        height -- Window height (ignored for type=='Full')
        detector -- Detector (overwritten by detector corresponding to wvl, if provided; must be provided for 'Full')
        slit -- Slit. Used only for default window shapes.
        xbin -- Bin factor in detector x (wavelength) direction

        Default width and height depend on window type and slit.

        "Spectral" means with any width in the spectral dimension: this also applies
        when a spatial information is expected rather than a spectral one (e.g.
        with the 30"-slit)

        If wvl is not given and type is not 'Full', the window can be used for evaluating resources
        but not for simulating spectra.
        '''

        # check window type
        if type not in Window.allowedTypes:
            raise ValueError('Window type should be one of ' + ' '.join(Window.allowedTypes))
        if type == 'SpectralDumbbells' and slit == Slit(30):
            warnings.warn('No dumbbells for 30" slit: reverting to "Spectral" window')
            type = 'Spectral'
        self.type = type

        # check wavelength and detector
        if wvl is not None:
            # get detector and centre pixel position
            detector = Detector(Detector.wvl2detector(wvl))
            self.pixCentre = detector.wvl2pix(wvl)  # detector keyword argument overwritten
        elif detector is None and type != 'Full':
            raise RuntimeError('Detector should be specified if wavelength is not specified')
        else:
            pass # TODO Following assertion fails systematically...
            # assert (isinstance(detector, Detector))
        self.detector = detector

        # check width and height
        dpix = detector.pixels()
        # defaults
        if type == 'Full':
            width = dpix[0]
            height = dpix[1]  # assumed to be already a multiple of 32
            wr = [wri.to(u.nm).value for wri in self.detector.wvlRange(fullArray=True)]
            wvl = np.mean(wr) * u.nm
            self.pixCentre = width // 2
        elif type == 'Spectral' or type == 'SpectralFullColumns' or type == 'SpectralDumbbells':
            if width is None:
                width = 32
            if height is None:
                if type == 'SpectralFullColumns':
                    height = detector.pixels()[1]
                else:
                    height = (slit.length() * detector.magy_px()).to(u.pix).value
                    height = int(np.round(height / 32)) * 32  # nearest multiple of 32
            if width not in Window.allowedWidths:
                warnings.warn('Window width not allowed')
        elif type == 'Intensity' or type == 'IntensityFullColumns':
            if width is None:
                width = 4
            if height is None:
                if type == 'IntensityFullColumns':
                    height = detector.pixels()[1]
                else:
                    height = (slit.length() * detector.magy_px()).to(u.pix).value
                    height = int(np.round(height / 32)) * 32  # nearest multiple of 32
        else:
            raise RuntimeError('Wrong window type (should have been checked before)')

        # check bin factor
        if type == 'Full':
            if xbin != 1:
                warnings.warn('Wavelength binning cannot be used in Full detector readout mode, reverting to no binning')
                xbin = 1
        else:
            if xbin not in [1, 2, 4, 8, 16, 32]:
                warnings.warn('Wavelength binning not in [1, 2, 4, 8, 16, 32], reverting to no binning')
                xbin = 1
            if width % xbin != 0:
                warnings.warn('Wavelength binning not dividing window width, reverting to no binning')
                xbin = 1
        self.xbin = xbin

        if width > dpix[0]:
            raise ValueError('Window width is more than detector width')
        if height > dpix[1]:
            raise ValueError('Window height is more than detector height')
        if width < 1:
            raise ValueError('Window width is too small')
        if height < 1:
            raise ValueError('Window height is too small')
        if height % 32 != 0:
            raise ValueError('Window height should be a multiple of 32 pixels')

        if type == 'Intensity' or type == 'IntensityFullColumns':
            if width not in [4, 8]:
                warnings.warn('Intensity window of width not in [4, 8] pix')
        elif type == 'SpectralDumbbells' or type == 'Dumbbells':
            if width != 32:
                warnings.warn('Width should be 32 pix for dumbbells: setting width to 32 pix')
                width = 32
        elif type != 'Full':
            if width not in [4, 8, 16, 32]:
                warnings.warn('Window width not in [4, 8, 16, 32] pix')

        self.width = width
        self.height = height

        # get pixel and wavelength ranges in wavelength direction
        # Assumes that wavelength corresponding to (integer) pixel position is at pixel centre
        if hasattr(self, 'pixCentre'):
            p0 = int(np.ceil(self.pixCentre - self.width / 2.))
            self.pixRange = [p0, p0 + self.width]  # including 1st, excluding 2nd
            if self.pixRange[0] < 0:  # shift window if at left edge of detector
                self.pixRange[1] += self.pixRange[0]
                self.pixRange[0] = 0
                warnings.warn('Window at detector edge, has been shifted')
            elif self.pixRange[1] > self.detector.pixels()[0]:
                self.pixRange[0] -= (self.pixRange[1] - self.detector.pixels()[0])
                self.pixRange[1] = 0
                warnings.warn('Window at detector edge, has been shifted')
            assert (self.pixRange[0] >= 0)
            assert (self.pixRange[1] <= self.detector.pixels()[0])
            self.wvlRange = (self.detector.pix2wvl(self.pixRange[0]),
                             self.detector.pix2wvl(self.pixRange[1], allowOutArray=True)
                             )
            self.wvlCentre = self.detector.pix2wvl(self.pixCentre)

        # add dumbbell information
        self.dbwidth = 32
        self.dbheight = 32
        if type == 'SpectralDumbbells' or type == 'Dumbbells':
            self.dbn = 2  # 2 dumbbells
        else:
            self.dbn = 0

        # this will be filled by spectrum()
        self.savedSpectrum = None

    def nPixels(self, output=False):
        '''Total number of pixels in window, in main window and in dumbbells

        Keyword parameters:
        output -- Return number of pixels in output window rather than on detector
            window (these are different for "Intensity" windows or for binned windows)
        '''
        # main window
        if output:
            if self.type == 'Intensity' or self.type == 'IntensityFullColumns':
                np = self.height
            else: np = self.width * self.height / self.xbin
        else:
            np = self.width * self.height
        # dumbbells (assumption: are not binned)
        npdb = self.dbwidth * self.dbheight * self.dbn
        return np, npdb

    def dataVolume(self, compressionRates):
        '''Total data volume from one exposure of the window (main window and dumbbells)

        Keyword parameters:
        compressionRates -- Compression rates
        '''
        np, npdb = self.nPixels(output=True)
        if self.type == 'Full':
            p = np / compressionRates.image
        elif self.type == 'Spectral' or self.type == 'SpectralFullColumns':
            p = np / compressionRates.spectrum
        elif self.type == 'SpectralDumbbells':
            p = np / compressionRates.spectrum + npdb / compressionRates.image
        elif self.type == 'Dumbbells':
            p = npdb / compressionRates.image
        elif self.type == 'Intensity' or self.type == 'IntensityFullColumns':
            p = np / compressionRates.image
        else:
            raise RuntimeError('Wrong window type (should have been checked before)')
        return p * 14 * u.bit  # 14bits/pix TBC

    @u.quantity_input
    def spectralRadiance2counts(self, wvl: u.nm, e: u.W / u.sr / u.m**3):
        '''Compute number of counts on detector pixels (per unit time) from spectral radiance

        Keyword parameters:
        -------------------
        wvl -- wavelength (in length unit)
        e -- spectral radiance

        TODO: check that slit width has already been taken into account (then input is not really a spectral radiance...)
        '''
        i = ((e * u.sr / u.rad ** 2               # (assuming sr/rad^2=1 for small angles)
              / self.detector.magx_px() * u.pix   # * horizontal angular pixel size
              / self.detector.magy_px() * u.pix   # * vertical angular pixel size
              * Detector.effectiveArea(wvl))      # * effective area
            .to(u.W / u.nm))                      # and simplify units

        # wavelength window irradiance over effective area (for DEBUG)
        #itot = scipy.integrate.cumtrapz (i.value, x=wvl.to(u.nm))[-1] * u.W

        # power received by each spectral pixel
        i *= self.detector.disp()
        # corresponding number of photon count per pixel per unit time
        i *= wvl / (const.h * const.c)
        return i.to(1 / u.s / u.pix)

    def spectrum(self, slit, elementList=None, ionList=None, demFile=None, add2nd=False):
        '''Predict spectrum in window using CHIANTI.

        Keyword parameters:
        -------------------
        slit -- slit
        elementList -- list of elements to be considered by CHIANTI
        ionList -- list of ions to be considered by CHIANTI
        demFile -- CHIANTI DEM file (without path or extension)
        add2nd -- If True, add 2nd order spectrum

        Return: wavelengths, counts/pix/sec, (start and end indices of pixels in window)
        or None if window has no position information.
        Saves result to self.savedSpectrum and to cache file.
        '''

        import ChiantiPy.core as ch
        import ChiantiPy.tools.filters as chfilters
        from .aux import readChiantiDEM
        import pickle

        # Spectrum cannot be computed if no position associated to window
        if not hasattr(self, 'wvlCentre'):
            self.savedSpectrum = None
            return None

        # maybe this spectrum has already been computed: return it
        if self.savedSpectrum is not None:
            p = self.savedSpectrum['params']
            # this condition is never met because p has been transformed...
            # TODO: check the right variables
            if (p['slitId'] == slit._id and p['width'] == self.width
                and p['wvlCentre'] == self.wvlCentre.value and p['demFile'] == demFile
                    and p['ionList'] == ionList and p['elementList'] == elementList):
                print('Using saved spectrum')
                return self.savedSpectrum
            # else:
                #print ('A saved spectrum exists but has not been used because of different')
                #if p['slitId'] != slit._id: print ('  slit')
                #if p['width'] != self.width: print ('  width')
                #if p['wvlCentre'] != self.wvlCentre: print ('  wvlCentre', p['wvlCentre'], self.wvlCentre)
                #if p['ionList'] != ionList: print ('  ionList', p['ionList'], ionList)
                #if p['elementList'] != elementList: print ('  elementList', p['elementList'], elementList)

        # look for pre-computed data in cache directory
        def cacheFile(params):
            if params['ionList'] is None:
                params['ionList'] = 'None'
            else:
                params['ionList'] = '-'.join(params['ionList'])
            if params['elementList'] is None:
                params['elementList'] = 'None'
            else:
                params['elementList'] = '-'.join(params['elementList'])
            if self.type == 'Full':
                params['wvlCentre'] = self.detector._id
            else:
                params['wvlCentre'] = params['wvlCentre'].to(u.nm).value
            if params['add2nd']:
                params['add2nd'] = 'T'
            else:
                params['add2nd'] = 'F'
            return os.path.join(cachePath,
                                'sl{slitId}_w{width}_wvl{wvlCentre}_2{add2nd}_dem-{demFile}_el-{elementList}_ion-{ionList}'.format(**params))

        cfile = cacheFile({'slitId': slit._id, 'width': self.width,
                           'wvlCentre': self.wvlCentre, 'add2nd': add2nd, 'demFile': demFile,
                           'elementList': elementList, 'ionList': ionList})
        if os.path.exists(cfile):
            with open(cfile, 'rb') as f:
                self.savedSpectrum = pickle.load(f)
            print('Using cached spectrum from ' + cfile)
            return self.savedSpectrum

        d = 1.e+9 / u.cm**3  # density
        t, em = readChiantiDEM(demFile)
        # force to CGS units for use by CHIANTI
        d = d.to(1 / u.cm**3)
        t = t.to(u.K)
        em = em.to(1 / u.cm**5)

        # wavelength array (in angstrom for use by CHIANTI)
        if self.type == 'Full':
            wvlIndex = np.arange(self.width)
        else:
            wvlExt = 16  # array extension on each side,
            wvlIndex = np.arange(-wvlExt, self.width + wvlExt)
        wvl = self.detector.pix2wvl(self.pixRange[0]) + \
            self.detector.disp() * wvlIndex * u.pix
        wvl = wvl.to(u.angstrom)

        # use resolving power (FWHM) for CHIANTI Gaussian filter width (standard deviation)
        wvlPSF = (self.wvlCentre / self.detector.spectres()
                  ).to(u.angstrom) / np.sqrt(8. * np.log(2.))
        # remove contribution of narrower slit to PSF (this is wrong if given PSF is optical PSF)
        # wvlPSF = np.sqrt (wvlPSF ** 2 - ((2.5 * u.arcsec * self.detector.magx_px() * self.detector.disp()).to (u.angstrom)) ** 2)

        # use CHIANTI to compute spectrum
        if ionList is None:
            chIonList = 0
        else:
            chIonList = ionList
        if elementList is None:
            chElementList = 0
        else:
            chElementList = elementList

        print('Computing spectrum with CHIANTI')
        # print 'T', t.value
        # print 'd', d.value
        # print 'wvl', wvl.value
        # print 'PSF', wvlPSF.value
        # print 'EM', em.value
        # print 'El', chElementList
        # print 'Ion', chIonList

        ab = 'sun_coronal_1999_fludra_ext'
        # first order spectrum
        s = ch.spectrum(t.value, d.value, wvl.value,
                        filter=(chfilters.gaussian, wvlPSF.value), em=em.value,
                        doContinuum=0, minAbund=0,  # 2e-5 for faster computation
                        elementList=chElementList, ionList=chIonList,
                        keepIons=False, abundance=ab)  # TODO use result of keepIons=True
        # spectral radiance, from CHIANTI output
        e = s.Spectrum['integrated'] * u.erg / u.s / u.cm**2 / u.sr / u.angstrom

        # wavelength window radiance (for DEBUG)
        #etot = scipy.integrate.cumtrapz (e.value, x=wvl.value)[-1] * u.erg / u.s / u.cm**2 / u.sr

        i = self.spectralRadiance2counts(wvl, e)

        # second order spectrum (2.000 denotes factors 2 at 2nd order)
        if add2nd:
            print('Computing 2nd order spectrum with CHIANTI')
            s2 = ch.spectrum(t.value, d.value, wvl.value / 2.000,
                             filter=(chfilters.gaussian, wvlPSF.value / 2.000), em=em.value,
                             doContinuum=0, minAbund=0,  # 2e-5 for faster computation
                             elementList=chElementList, ionList=chIonList,
                             keepIons=False, abundanceName=ab)  # TODO use result of keepIons=True
            # spectral radiance, from CHIANTI output
            e2 = s2.Spectrum['integrated'] * u.erg / u.s / u.cm**2 / u.sr / u.angstrom
            i2 = self.spectralRadiance2counts(wvl, e2)
            # factors for 2nd order:
            # .333  Assumed effective area factor for 2nd order (see SPICE-RAL-RP-0002 p.48 for 50nm)
            # .5    Dispersion for 2nd order is half dispersion for 1st order
            # .5    Each photon has twice the energy of a photon arriving on same pixel from 1st order
            i2 *= .333 * .5 * .5
            i += i2  # sum 2nd order to 1st order spectrum

        # all this assumed a slit width corresponding to 1 pixel; now use real slit width
        # (units of i were 1/pix/s, but are lost with np.convolve)
        slWidthPix = (slit.width() * self.detector.magx_px()).to(u.pix).value
        i = np.convolve(i.value, symmetricOnes(slWidthPix), mode='same') / (u.pix * u.s)
        # save for future use
        self.savedSpectrum = dict()
        self.savedSpectrum['wvl'] = wvl
        self.savedSpectrum['wvlIndex'] = wvlIndex
        self.savedSpectrum['counts'] = i
        self.savedSpectrum['slitWidth'] = slit.width()
        self.savedSpectrum['params'] = {'slitId': slit._id, 'width': self.width,
                                        'wvlCentre': self.wvlCentre, 'add2nd': add2nd, 'demFile': demFile,
                                        'elementList': elementList, 'ionList': ionList}

        # save result to cache
        cfile = cacheFile(self.savedSpectrum['params'])
        if not os.path.exists(cachePath):
            os.mkdir(cachePath)
        with open(cfile, 'wb') as f:
            pickle.dump(self.savedSpectrum, f)

        return self.savedSpectrum

    def plotSpectrum(self, slit=None, exptime=None, elementList=None, ionList=None,
                     demFile=None, add2nd=False, ax=None, logy=False, factor=1.,
                     showWidth=True, showAtlas=True, **kwargs):
        '''Plot spectrum in window

        Keyword parameters:
        -------------------
        slit -- slit
        exptime -- exposure time, or None to plot spectrum in counts/s
        elementList -- list of elements to be considered by CHIANTI
        ionList -- list of ions to be considered by CHIANTI
        demFile -- CHIANTI DEM file (without path or extension)
        add2nd -- If True, add 2nd order spectrum
        ax -- plot on these axes (if provided)
        logy -- logarithmic y axis
        factor -- multiply by this factor
        showWidth -- show indication of line width origins
        showAtlas -- show spectal atlas data
        **kwargs -- arguments passed to spectrum plotting
            (allows to change default plotting style)
        '''

        # compute spectrum (if not already done or cached)
        self.spectrum(slit, elementList=elementList, ionList=ionList,
                      demFile=demFile, add2nd=add2nd)
        if self.savedSpectrum is None:
            warnings.warn('Cannot plot spectrum of a window with no position')
            return

        # spectrum as function of wavelength
        if ax is None:
            fig, ax = plt.subplots()
            doShow = True
        else:
            doShow = False
        factor *= (1 if exptime is None else exptime)
        s = self.savedSpectrum['counts'] * factor
        if logy:
            s += 1e-10 / u.pix / u.s * factor  # avoid too small values in log scale
        ax.plot(self.savedSpectrum['wvl'], s, **kwargs)
        if logy:
            ax.set_yscale('log')
        ax.autoscale(axis='x', tight=True)
        ax.set_xlabel(u'Wavelength [{}]'.format(self.savedSpectrum['wvl'][0].unit))
        ax.set_ylabel('Photon counts/pix{}'.format('/s' if exptime is None else ''))
        # spectrum as function of pixel index, on 2nd x axis
        ax2 = ax.twiny()
        ax2.plot(self.savedSpectrum['wvlIndex'], s, 'r.', **kwargs)
        ax2.set_xlabel('Window pixel index', color='r')
        for tl in ax2.get_xticklabels():
            tl.set_color('r')

        # plot photon noise (relevant only if some exposure time is set)
        if exptime is not None:
            from scipy.stats import poisson
            # Poisson distribution intervals for 50% and 90% of realizations
            noiseFill = {'color': 'b', 'alpha': 0.07}
            noiseLevels = [50, 90]
            for a in noiseLevels:
                sint = poisson.interval(a / 100., s)
                ax2.fill_between(self.savedSpectrum['wvlIndex'], sint[0], sint[1],
                                 **noiseFill)
            # a few realizations
            if False:
                for i in range(10):
                    sr = [poisson.rvs(si) for si in s]
                    ax2.plot(self.savedSpectrum['wvlIndex'], sr, 'k-', alpha=.1)
        ax2.autoscale(axis='x', tight=True)
        ax2.grid()

        # shade wavelength range out of selected window
        yl = ax2.get_ylim()
        mini, maxi = np.min(self.savedSpectrum['wvlIndex']), np.max(self.savedSpectrum['wvlIndex'])
        if mini <= -.5:  # avoid extending plotting range (would shift plots on ax and ax2)
            ax2.fill_between([mini, -.5], yl[1], color='k', alpha=.1)
        if self.width - .5 <= maxi:  # also avoid extending plotting range
            ax2.fill_between([self.width - .5, maxi], yl[1], color='k', alpha=.1)

        # indications for PSF, slit width, thermal width
        if showWidth:
            wvlPSF = (self.wvlCentre / self.detector.spectres()).to(u.angstrom).value
            wvlSlitWidth = (
                self.savedSpectrum['slitWidth'] * self.detector.magx_px() * self.detector.disp()).to(u.angstrom).value
            vThermalFe1MK = np.sqrt(3 * const.k_B * 1e6 * u.K / (55.845 * const.m_p))
            wvlThermalFe1MK = (vThermalFe1MK / const.c * self.wvlCentre).to(u.angstrom).value
            xl, yl = ax.get_xlim(), ax.get_ylim()
            x0 = xl[0] + 0.05 * (xl[1] - xl[0])
            y0 = yl[0] + 0.90 * (yl[1] - yl[0])
            dy = (yl[1] - yl[0]) * 0.001
            y0text = y0 + 0.01 * (yl[1] - yl[0])
            ax.plot([x0, x0 + wvlPSF], [y0] * 2, 'm-|')
            ax.annotate('PSF', (x0 + wvlPSF / 2., y0text), ha='right')
            x0 += wvlPSF
            ax.plot([x0, x0 + wvlSlitWidth], [y0 + dy] * 2, 'c-|')
            ax.annotate('Slit', (x0 + wvlSlitWidth / 2., y0text), ha='center')
            x0 += wvlSlitWidth
            ax.plot([x0, x0 + wvlThermalFe1MK], [y0] * 2, 'r-|')
            ax.annotate('Fe 1MK', (x0 + wvlThermalFe1MK / 2., y0text), ha='left')
            # legend for photon noise bands
            x0 = xl[0] + 0.05 * (xl[1] - xl[0])
            y0 = yl[0] + 0.85 * (yl[1] - yl[0])
            dx = 0.05 * (xl[1] - xl[0])
            dy = 0.03 * (yl[1] - yl[0])
            if exptime is not None:
                p = plt.Rectangle((x0, y0), dx, dy, **noiseFill)
                ax.add_patch(p)
                p = plt.Rectangle((x0 + 0.2 * dx, y0 + 0.2 * dy), 0.6 * dx, 0.6 * dy, **noiseFill)
                ax.add_patch(p)
                ax.annotate('{}% photons'.format(
                    '/'.join([str(n) for n in noiseLevels])), (x0 + 1.2 * dx, y0))

        # plot line information from Curdt et al. 01 if available
        # (show only if plot width less than 10Å)
        xrange = ax.get_xaxis().get_data_interval()
        yrange = ax.get_yaxis().get_data_interval()
        if showAtlas:   # and np.abs (xrange[1] - xrange[0]) < 10):
            try:             # region to use depends on DEM file
                region = {'quiet_sun': 'QS', 'coronal_hole': 'CH', 'active_region': 'SS'}[demFile]
            except KeyError:
                region = 'SS'  # default region
            atlas = getCurdt01Atlas()
            for r in atlas:
                if not r['L' + region]:
                    continue  # line not detected
                wvl = r['Lambda'] * u.angstrom
                if (wvl > self.wvlRange[0] and wvl < self.wvlRange[1]):
                    order = ''      # first-order line
                elif (2. * wvl > self.wvlRange[0] and 2. * wvl < self.wvlRange[1]):
                    order = ' (2)'  # second-order line
                else:
                    continue        # wavelength not in plot
                # peak spectral radiance
                e = r['L' + region] * 10. * u.mW / u.sr / u.m**2 / u.nm  # convert to SI
                counts = self.spectralRadiance2counts(wvl, e).value
                if exptime is not None:
                    counts *= exptime.to(u.s).value
                if (counts > yrange[1]):  # clip to plot y axis
                    counts = yrange[1]
                    clipped = ' (cl)'
                else:
                    clipped = ''
                ax.annotate(str(r['Line']) + order + clipped,
                            (wvl.value, counts), ha='center', va='baseline')
                ax.plot(wvl.value, counts, 'r+')

        if doShow:
            plt.show()

    def totCounts(self, slit=None, exptime=None, elementList=None, ionList=None,
                  demFile=None, add2nd=False):
        '''Total counts (or counts/s if exptime is None) in window'''
        s = self.spectrum(slit=slit, elementList=elementList,
                          ionList=ionList, demFile=demFile, add2nd=add2nd)
        factor = (1 if exptime is None else exptime)
        ir = np.abs(s['wvlIndex']).argmin(), np.abs(s['wvlIndex'] - self.width + 1).argmin() + 1
        return (np.sum(s['counts'][ir[0]:ir[1]])) * factor * u.pix

    def __str__(self):
        '''String for printing window information'''
        s = 'Window type {} ({}x{})'.format(
            self.type, self.width, self.height
        )
        if self.dbn > 0:
            s += '. {} dumbells ({}x{})'.format(
                self.dbn, self.dbwidth, self.dbheight
            )
        if hasattr(self, 'pixCentre'):
            s += ' {}, pixels {}-{} ({})'.format(self.detector.__str__(),
                                                 self.pixRange[0], self.pixRange[1] - 1, self.wvlCentre.to(u.nm))
        else:
            s += ' {} (position unspecified)'.format(self.detector.__str__())
        return s
