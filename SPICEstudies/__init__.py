#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
SPICE studies definition and simulation

Licence: GPLv3
Author: Eric Buchlin, eric.buchlin@ias.u-psud.fr

@package SPICEstudies
'''
from .slit import *
from .detector import *
from .compressionrates import *
from .window import *
from .exposure import *
from .raster import *
from .study import *

import os.path
SPICEstudiesPath = os.path.dirname(os.path.abspath(__file__))
cachePath = os.path.join(SPICEstudiesPath, 'cache')
