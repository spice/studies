#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Auxiliary functions for SPICEstudies'''
from builtins import range
from astropy import units as u
from astropy import constants as const
import numpy as np
from scipy.interpolate import interp1d
import os
import warnings

from .detector import Detector


def readChiantiDEM(file, temp=None, dir=None, integrate=True):
    '''Read CHIANTI DEM file to get emission measure in ranges of temperature.

    Parameters
    ----------
    file: string
        CHIANTI DEM file name (without directory or extension)
    dir: list of strings
        Directories where DEM files are searched. Default is subdirectory dem/ of system CHIANTI database directory (XUVTOP system variable)
    temp: array
        Array of desired output temperatures
    integrate: bool
        If True (default), integrate over temperature bins to get EM(T) instead of DEM(T)

    Return
    ------
    t: numpy array
        Temperatures
    em: numpy array
        Emission measures

    '''

    DemPath = os.path.join(
        os.environ.get('XUVTOP', '/usr/local/ssw/packages/chianti/dbase'),
        'dem')

    if dir is None:
        dir = [DemPath]
    elif isinstance(dir, list):
        pass
    else:
        dir = [dir]
    if file is None:
        file = 'active_region'

    for d in dir:
        pfile = os.path.join(d, file + '.dem')
        if not os.path.exists(pfile):
            continue

        # read DEM file
        data = []
        with open(pfile, 'r') as f:
            for l in f.readlines():
                if l.split() == ['-1']:
                    break
                data.append([np.float32(ll) for ll in l.split()])
        data = np.array(data)
        logt = data[:, 0]
        logdem = data[:, 1]

        if temp is not None:
            from decimal import Decimal
            logdemt = interp1d(logt, logdem, bounds_error=False, fill_value=Decimal('-Infinity'))
            logt = np.log10(temp)
            logdem = logdemt(logt)
            if np.any(logdem == Decimal('-Infinity')):
                warnings.warn(
                    '0 in interpolated DEM: temperature probably out of range of CHIANTI DEM file')

        if integrate:
            em = np.zeros_like(logdem)
            # integrate DEM on each T interval to get EM(T)
            for i in range(len(logt)):
                # T interval
                if i != 0:
                    t0 = 10. ** ((logt[i-1] + logt[i]) / 2.)
                else:
                    t0 = 10. ** (logt[i] - (logt[i+1] - logt[i]) / 2.)
                if i != len(logt) - 1:
                    t1 = 10. ** ((logt[i] + logt[i+1]) / 2.)
                else:
                    t1 = 10. ** (logt[i] + (logt[i] - logt[i-1]) / 2.)
                em[i] = 10. ** logdem[i] * (t1 - t0)
            return 10. ** logt * u.K, em / u.cm**5
        else:
            return 10. ** logt * u.K, 10. ** logdem / u.cm**5 / u.K

    raise RuntimeError('DEM file not found, please check path for CHIANTI database')  # DemPath


def getCurdt01Atlas():
    #url = 'http://cdsarc.u-strasbg.fr/vizier/ftp/cats/J/A+A/375/591/tablea1.dat.gz'
    #cacheFile = cachePath + '/cache/curdt01.dat.gz'
    try:
        from astroquery.vizier import Vizier
    except ImportError:
        warnings.warn('astroquery.vizier module not available: cannot get Curdt et al. 2001 line list')
        return []
    Vizier.ROW_LIMIT = -1
    try:
        table = Vizier.get_catalogs('J/A+A/375/591')['J/A+A/375/591/tablea1']
    except (Exception, reason):
        warnings.warn('Could not get Curdt et al. 2001 line list: ' + reason)
        return []
    return table


def indent(s, n=2):
    '''Indent text in string (add spaces at start of each line)

    `textwrap.indent(s, ' ' * n)` could be used instead, for Python > 3.3

    Parameters
    ----------
    s: string
        String to be indented
    n: int
        Number of spaces to use for indent
    '''
    return ''.join(' ' * n + line for line in s.splitlines(True))


def radiance2counts(e, wvl, slit):
    '''Compute number of counts on detector pixels (per unit time) from radiance (integrated over wavelength)

    Keyword parameters
    ------------------
    e -- radiance (in radiance unit, convertible to W/sr/m²)
    wvl -- wavelength (in length unit)
    slit -- slit (Slit object)
    '''

    detector = Detector(Detector.wvl2detector(wvl))
    i = (e * u.sr / u.rad ** 2                # (assuming sr/rad^2=1 for small angles)
         * slit.width()                        # * slit width
         / detector.magy_px()                  # * vertical angular pixel size
         * Detector.effectiveArea(wvl)) \
             .to(u.W / u.pix)                  # * and simplify units
    # wavelength window irradiance over effective area (for DEBUG)
    #itot = scipy.integrate.cumtrapz (i.value, x=wvl.to(u.nm))[-1] * u.W

    # corresponding number of photon count per pixel per unit time
    i *= wvl / (const.h * const.c)
    return i.to(1 / u.s / u.pix)


@u.quantity_input
def radianceSNR2exptime(wvl: u.nm,
                        slit: u.arcsec,
                        ny: u.pix,
                        L,
                        snr,
                        star=False,
                        phot=False):
    '''
    Compute exposure time required for a given line radiance and SNR (with same assumptions than Alessandra)

    Parameters
    ----------
    wvl: Quantity
        Line wavelength
    slit: Quantity
        Slit (angular) width
    tot_slit_ypix: Quantity
        Number of y pixels (for rebin)?
    L: Quantity
        For extended source: line radiance [W/sr/m2].
        For point source: line radiant intensity [W/sr]
        Can be given in photons instead of energy (see `phot`).
    snr: float
        Mean signal to noise ratio in line
    star: bool
        True for point source (as star)
    phot: bool
        True if L is given in photons instead of energy

    See SPICE-RAL-RP-0002 v7 8.7.2.
    '''
    # instrument parameters
    p = {
        'gain': 7.5 * u.electron / u.ph,  # MCP gain
        'MCPnoiseMult': 1.,               # MCP noise multiplication factor
        'PSFlambda': 3.6 * u.pix,         # PSF in wavelength
        'PSFy': 4.7 * u.pix,              # PSF in y
        'darkCurrent': 2.4 * u.electron / u.s / u.pix**2, # dark current in DNs
        'readNoise': 6.9 / u.pix**2,
        'saturationLevel': 14e4,
        }
    # background signal is assumed to be 1 ph/px/s (in DNs)
    dndtBg = (1 * u.ph / u.pix**2 / u.s) \
        * Detector.quantumEfficiency(wvl)
    print('Background signal: {:0.4}'.format(dndtBg))
    if not star:  # integrate over field of view
        # assume 1-arcsec y pixel; TODO: use real pixel size / magnification
        L *= slit * ny * u.arcsec / u.pix
        if phot:
            print('Extended source photon flux is {:0.4}'.format(L))
    if not phot:  # convert energy to number of photons
        L /= (const.h * const.c / wvl) / u.ph
    L <<= u.ph / u.s / u.m**2   # in-place conversion
    print('Photon flux: {:0.4}'.format(L))
    # Signal ph/s
    ea = Detector.effectiveArea(wvl)
    dNdtSig = (L * ea) << u.ph / u.s
    print('Effective area: {:0.4}'.format(ea))
    print('Photon rate: {:0.4}'.format(dNdtSig))
    # Mean signal over pixels in line, ph/s/pix
    # TODO: why mean signal?
    # TODO: divide by y-PSF only for star?
    dndtSig = dNdtSig / (p['PSFlambda'] * p['PSFy'])
    print('Mean photon rate per pixel: {:0.4}'.format(dndtSig))

    # Solve 2nd-degree equation to get exposure time from target SNR
    # TODO get powers of dimensionless units (photons, electrons) right...
    renormalizeDimensionless = True
    if renormalizeDimensionless:
        dndtSig /= u.ph
        p['gain'] /= u.electron / u.ph
        p['darkCurrent'] /= u.electron
        dndtBg /= u.electron
        p['readNoise'] *= u.pix**2

    eqA = (p['gain'] * dndtSig)**2
    eqB = -snr**2 * ((p['MCPnoiseMult'] * p['gain'])**2 * dndtSig
                     + p['darkCurrent']
                     + p['gain']**2 * dndtBg)
    eqC = -snr**2 * p['readNoise']**2
    eqDelta = eqB**2 - 4 * eqA * eqC
    t = (-eqB + np.sqrt(eqDelta)) / (2 * eqA) / u.pix**2
    print('Exposure time for target SNR at peak: {:0.4}'.format(t))
    print('Exposure time for target SNR integrated: {:0.4}'.format(t / np.sqrt(p['PSFlambda'] * p['PSFy']) * u.pix))
